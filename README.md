## Selenium Chrome HTTP Private Proxy (Java)
Start selenium browser with proxy.

Use proxy with a basic authentication with Chrome and Selenium.

This Plugin permit to use proxy with a basic authentication with Chrome and Selenium.

**Chrome Headless doesn't work! See more: https://bugs.chromium.org/p/chromium/issues/detail?id=706008#c5**
### Parameters
* Set CHROMEDRIVER_FILE_FULL_PATH in pampushko.java.selenium.chrome.proxy.Main
* Set proxy settings properties in package pampushko.java.selenium.chrome.proxy.ProxySettings

 
## Motivations 
By default, Chrome use the system proxy setting (IE proxy settings on Windows platform ), but sometime we want to set proxy ONLY for chrome, not the whole system.
 
 I am not able to find any documentation on **how to set proxy username and password for Chrome webdriver**.


## Contributors 
Alexander Pampushko
 
## License
BSD license
