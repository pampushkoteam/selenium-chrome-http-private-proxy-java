package pampushko.java.selenium.chrome.proxy;

import com.google.common.io.Resources;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class Main
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	public static final String TEMPLATE = "chrome_plugin_sources/background.js";
	public static final String PLUGIN_FILENAME = "proxy_plugin.zip";
	public static final String CHROMEDRIVER_FILE_FULL_PATH = "/home/username/drivers/chromedriver";
	
	public static void main(String[] args)
	{
		//Получаем файл шаблона.
		//Файл содержит вместо настроек прокси - переменные, которые шаблонизатор заменит конкретными значениями
		Path templatePath = Paths.get(Resources.getResource(TEMPLATE).getPath());
		System.out.println(templatePath);
		
		//получаем настройки прокси
		ProxySettings proxySettings = new ProxySettings();
		
		//генерируем новый файл, который мы включим в плагин
		String pluginFile = Replacer.generateFileForPlugin(templatePath, proxySettings);
		System.out.println(pluginFile);
		
		//формируем список всех файлов, которые должны быть упакованы в архив поставки плагина
		List<Path> pluginFiles = FilesManager.makeFileListOfPlugin(pluginFile);
		System.out.println(pluginFiles);
		
		//Запаковываем все нужные файлы в архив
		//Полученный архив, это уже готовый к установке плагин :)
		String pluginPackage = FilesManager.makePluginZipFileFromFileList(pluginFiles, PLUGIN_FILENAME);
		System.out.println(pluginPackage);
		
		runExample(pluginPackage);
	}
	
	/**
	 * Запускаем браузер предварительно установив дополнение из файла pluginZipFileName.
	 * <br>
	 * После этого все наши обращения к интернет ресурсам будут идти через прокси-сервер.
	 * <br>
	 * При этом браузер не будет выводит окно с требованием авторизации на прокси-сервере - эти параметры уже указаны в дополнении.
	 * <br>
	 *
	 * @param pluginZipFileName
	 * 		полное имя файла с дополнением
	 */
	private static void runExample(String pluginZipFileName)
	{
		try
		{
			// Optional, if not specified, WebDriver will search your path for chromedriver.
			System.setProperty("webdriver.chrome.driver", CHROMEDRIVER_FILE_FULL_PATH);
			
			WebDriver driver = new ChromeDriver(
					new ChromeOptions()
							.addExtensions(new File(pluginZipFileName))
					//.addArguments("--headless") //Runs Chrome in headless mode.
					//HEADLESS CHROME DOES NOT SUPPORT EXTENSIONS.
					//see more: https://bugs.chromium.org/p/chromium/issues/detail?id=706008#c5
					//.addArguments("disable-gpu"); //Temporarily needed if running on Windows.
			);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
			driver.get("https://vk.com/");
			Thread.sleep(5000); // Let the user actually see something!
			driver.quit();
			
		}
		catch (Exception ex)
		{
			log.error("webdriver error", ex);
		}
	}
}
