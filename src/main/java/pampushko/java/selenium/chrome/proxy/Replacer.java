package pampushko.java.selenium.chrome.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STErrorListener;
import org.stringtemplate.v4.misc.ErrorBuffer;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 *
 */
public class Replacer
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	/**
	 * Читаем файл, заменяем в теле файла параметры значениями настроек прокси,
	 * <br>
	 * Создаем и записываем новый файл с установленными значениями параметров прокси
	 * <br>
	 *
	 * @param path путь к файлу, в котором надо выполнить замену
	 * @return полное имя файла
	 */
	public static String generateFileForPlugin(Path path, ProxySettings proxySettings)
	{
		String tempFileName = "";
		try
		{
			String sourceString = new String(Files.readAllBytes(path));
			ST template = new ST(sourceString);
			
			template.add("proxy_host", proxySettings.host);
			template.add("proxy_port", proxySettings.port);
			template.add("username", proxySettings.username);
			template.add("password", proxySettings.password);
			template.add("all_urls", "<all_urls>"); //todo тут мы пересекаемся с синтаксисом шаблонов
			
			//создаём временный файл
			tempFileName = FilesManager.createTempFile("background.js");
			STErrorListener errors = new ErrorBuffer();
			
			template.write(new File(tempFileName), errors);
			//log.debug("{}", template.render());
		}
		catch (IOException ex)
		{
			log.error(ex.getMessage());
		}
		
		return tempFileName;
	}
}
