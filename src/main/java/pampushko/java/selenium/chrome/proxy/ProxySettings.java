package pampushko.java.selenium.chrome.proxy;

/**
 * Класс, поля которого содержат набор значений настроек прокси сервера
 * <br>
 */
public class ProxySettings
{
	/**
	 * адрес хоста
	 */
	public String host = "proxy.com";
	
	/**
	 * строка, значение которой затем будет парситься в int - номер порта
	 */
	public String port = "3128";
	
	/**
	 * имя пользователя для basic auth в прокси
	 */
	public String username = "user";
	
	/**
	 * пароль пользователя для basic auth в прокси
	 */
	public String password = "password";
}
