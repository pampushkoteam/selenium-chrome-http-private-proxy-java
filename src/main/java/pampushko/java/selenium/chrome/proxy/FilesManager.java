package pampushko.java.selenium.chrome.proxy;

import com.google.common.io.Files;
import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Класс содержит впомогательные методы для работы с файлами
 */
public class FilesManager
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	/**
	 * Метод создает временный файл по указанному вами имени.
	 * <br>
	 * Перед созданием файла метод создает временную директорию.
	 * <br>
	 * В этой директории затем создаётся временный файл.
	 * <br>
	 *
	 * @param fileName
	 * 		имя файла, который мы хотим создать
	 *
	 * @return полное имя уже созданного файла
	 */
	public static String createTempFile(String fileName)
	{
		File myTempDir = Files.createTempDir();
		log.info("Create temp dir {}", myTempDir);
		String resultFileName = myTempDir.getPath() + File.separator + fileName;
		log.info("generate full path to file {}", resultFileName);
		return resultFileName;
	}
	
	/**
	 * Формируем список файлов, которые будут использоваться для создания Chrome-плагина
	 *
	 * @param backgroundJsFullFileName
	 * 		полное имя файла (background.js) с динамически генерируемым содержимым, который мы также хотим добавить в Chrome-плагин
	 *
	 * @return список объектов Path, каждый из которых соответствует одному из добавляемых в плагин файлов
	 */
	public static List<Path> makeFileListOfPlugin(String backgroundJsFullFileName)
	{
		Path file_backgroundJs = Paths.get(backgroundJsFullFileName);
		Path file_manifestJs = Paths.get(Resources.getResource("chrome_plugin_sources/manifest.json").getFile());
		
		List<Path> filesList = new ArrayList<Path>()
		{
			{
				add(file_backgroundJs);
				add(file_manifestJs);
			}
		};
		return filesList;
	}
	
	/**
	 * Метод на основании списка файлов fileList
	 * <br>
	 * создаёт архив, котором упакованы все эти файлы.
	 * Полученный архив - это файл поставки плагина,
	 * <br>
	 * который может быть непосредственно установлен в Chrome
	 * <br>
	 * Архив помещается во временную директорию и временный файл.
	 * <br>
	 *
	 * @param fileList
	 * 		список передаваемых для упаковки файлов
	 * @param archiveNameWithExtension
	 * 		имя архива (также необходимо указать в имени расширение .zip)
	 *
	 * @return полное имя (путь к файлу + имя файла) созданного файла-архива
	 */
	public static String makePluginZipFileFromFileList(List<Path> fileList, String archiveNameWithExtension)
	{
		String resultZipFullFilename = "";
		FileInputStream fin = null;
		ZipOutputStream zipOut = null;
		try
		{
			resultZipFullFilename = createTempFile(archiveNameWithExtension);
			FileOutputStream fileOutputStream = new FileOutputStream(resultZipFullFilename);
			
			zipOut = new ZipOutputStream(fileOutputStream);
			
			for (Path path : fileList)
			{
				ZipEntry entry = new ZipEntry(path.getFileName().toString());
				
				fin = new FileInputStream(path.toFile());
				
				byte[] b = new byte[1024];
				zipOut.putNextEntry(entry);
				int length;
				while ((length = fin.read(b, 0, 1024)) > 0)
				{
					zipOut.write(b, 0, length);
				}
			}
		}
		catch (IOException ex)
		{
			log.error(ex.getMessage());
		}
		finally
		{
			if (fin != null)
			{
				try
				{
					fin.close();
					zipOut.close();
				}
				catch (IOException ex)
				{
					log.error(ex.getMessage());
				}
			}
		}
		
		//возвращаем полное имя созданного файла-архива
		return resultZipFullFilename;
	}
}
